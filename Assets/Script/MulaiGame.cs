﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MulaiGame : MonoBehaviour
{
    public void NewGame()
    {
        SceneManager.LoadScene("MainLevel01");
    }
    public void LoadGame()
    {
        if(PlayerPrefs.GetInt("LoadSaved") == 8)
        {
            SceneManager.LoadScene(PlayerPrefs.GetInt("SavedScene"));
        }
        else
        {
            return;
        }
    }
}
