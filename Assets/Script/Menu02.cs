﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Menu02 : MonoBehaviour
{
    public static Menu02 menu02;
    public TMP_InputField inputField, inputField2;
    public string player_name, Gender;
    public TextMeshProUGUI ErrorMessage_Holder;

    const string error_message = "Harus di isi dulu yaa!";



    private void Awake()
    {
        if (menu02 == null)
        {
            menu02 = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void SetPlayerName()
    {
        if (string.IsNullOrEmpty(inputField2.text) && string.IsNullOrEmpty(inputField.text))
        {
            ErrorMessage_Holder.text = error_message;
        }
        else
        {
            player_name = inputField.text;
            Gender = inputField2.text;
            SceneManager.LoadSceneAsync("MainMenu");
        }


    }
}
