﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MainMenu : MonoBehaviour
{

    public TextMeshProUGUI display_player_name, display_gender;

    public void Awake()
    {
        display_player_name.text = Menu02.menu02.player_name;
        display_gender.text = Menu02.menu02.Gender;
    }

}
