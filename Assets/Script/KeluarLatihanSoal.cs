﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KeluarLatihanSoal : MonoBehaviour
{
    // Info Keluar Latiahan Soal

    public GameObject KeluarKuis;
    public void KeluarKuisini()
    {
        SceneManager.LoadSceneAsync("MainMenu");
    }
    public void ButtonTampil()
    {
        KeluarKuis.SetActive(true);
    }
    public void Buttontidakjadi()
    {
        KeluarKuis.SetActive(false);
    }
}
