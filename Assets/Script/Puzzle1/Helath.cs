﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Helath : MonoBehaviour
{
    public int Health;
    public GameObject Nyawa1, Nyawa2, Nyawa3, Nyawa4, Nyawa5, Kalah;
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {

        if(Health == 5)
        {
            Nyawa1.SetActive(true);
            Nyawa2.SetActive(true);
            Nyawa3.SetActive(true);
            Nyawa4.SetActive(true);
            Nyawa5.SetActive(true);
        }
        else if (Health == 4)
        {
            Nyawa1.SetActive(true);
            Nyawa2.SetActive(true);
            Nyawa3.SetActive(true);
            Nyawa4.SetActive(true);
            Nyawa5.SetActive(false);
        }
        else if (Health == 3)
        {
            Nyawa1.SetActive(true);
            Nyawa2.SetActive(true);
            Nyawa3.SetActive(true);
            Nyawa4.SetActive(false);
            Nyawa5.SetActive(false);
        }
        else if (Health == 2)
        {
            Nyawa1.SetActive(true);
            Nyawa2.SetActive(true);
            Nyawa3.SetActive(false);
            Nyawa4.SetActive(false);
            Nyawa5.SetActive(false);
        }
        else if (Health == 1)
        {
            Nyawa1.SetActive(true);
            Nyawa2.SetActive(false);
            Nyawa3.SetActive(false);
            Nyawa4.SetActive(false);
            Nyawa5.SetActive(false);
        }
        else if (Health == 0)
        {
            Nyawa1.SetActive(false);
            Nyawa2.SetActive(false);
            Nyawa3.SetActive(false);
            Nyawa4.SetActive(false);
            Nyawa5.SetActive(false);
            Kalah.SetActive(true);
        }
    }

}

