﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class Slot2 : MonoBehaviour, IDropHandler
{
    public GameObject feed_benar, feed_salah;
    public Helath KomponenHealth;
    public Poin2 KomponenPoin;
    public int id;
    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("Item Dropped");
        if (eventData.pointerDrag != null)
        {
            if (eventData.pointerDrag.GetComponent<DragAndDrop2>().id == id)
            {
                eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = GetComponent<RectTransform>().anchoredPosition;
                feed_benar.SetActive(false);
                feed_benar.SetActive(true);
                Poingame();
            }
            else
            {
                Debug.Log("Salah");
                eventData.pointerDrag.GetComponent<DragAndDrop2>().resetposisi();
                feed_salah.SetActive(false);
                feed_salah.SetActive(true);
                Health();
            }
        }

    }

    void Health()
    {
        KomponenHealth.Health--;
    }
    void Poingame()
    {
        KomponenPoin.Poinku++;
    }
}
