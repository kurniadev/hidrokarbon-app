﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Nyawa : MonoBehaviour
{


    public int health ;
    public int numOfHearts;
    public Image[] hearts;
    public Sprite fullHearts;
    public Sprite emptyHearts;

    public void Nyawaku()
    {
        numOfHearts--;
    }

    void Start()
    {
        health = 3;
        numOfHearts = 3;

    }
    void Update()
    {


       


        for (int i = 0; i <hearts.Length; i++)
        {
            if(i < health)
            {
                hearts[i].sprite = fullHearts;
            }
            else
            {
                hearts[i].sprite = emptyHearts;
            }

            if (i < numOfHearts)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }
        }    
    }
}
