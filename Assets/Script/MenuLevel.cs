﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuLevel : MonoBehaviour
{
    public void Level1()
    {
        SceneManager.LoadSceneAsync("Level1");
    }
    public void Level2()
    {
        SceneManager.LoadSceneAsync("Level2");
    }
    public void Level3()
    {
        SceneManager.LoadSceneAsync("Level3");
    }
    public void Level4()
    {
        SceneManager.LoadSceneAsync("Level4");
    }
    public void Level5()
    {
        SceneManager.LoadSceneAsync("Level5");
    }
    public void Level6()
    {
        SceneManager.LoadSceneAsync("Level6");
    }
    public void Level7()
    {
        SceneManager.LoadSceneAsync("Level7");
    }
    public void Level8()
    {
        SceneManager.LoadSceneAsync("Level8");
    }
    public void Level9()
    {
        SceneManager.LoadSceneAsync("Level9");
    }
    public void Level10()
    {
        SceneManager.LoadSceneAsync("Level10");
    }
    public void Level11()
    {
        SceneManager.LoadSceneAsync("Level11");
    }
    public void Level12()
    {
        SceneManager.LoadSceneAsync("Level12");
    }
    public void Level13()
    {
        SceneManager.LoadSceneAsync("Level13");
    }
    public void Level14()
    {
        SceneManager.LoadSceneAsync("Level14");
    }
    public void Level15()
    {
        SceneManager.LoadSceneAsync("Level15");
    }

    // MenuBaganLevel

    public void MainLevel1()
    {
        SceneManager.LoadSceneAsync("MainLevel01");
    }
    public void MainLevel2()
    {
        SceneManager.LoadSceneAsync("MainLevel02");
    }
    public void MainLevel3()
    {
        SceneManager.LoadSceneAsync("MainLevel03");
    }
    public void MainLevel4()
    {
        SceneManager.LoadSceneAsync("MainLevel04");
    }
    public void MainLevel5()
    {
        SceneManager.LoadSceneAsync("MainLevel05");
    }
    public void MainLevel6()
    {
        SceneManager.LoadSceneAsync("MainLevel06");
    }
    public void MainLevel7()
    {
        SceneManager.LoadSceneAsync("MainLevel07");
    }
    public void MainLevel8()
    {
        SceneManager.LoadSceneAsync("MainLevel08");
    }
    public void MainLevel9()
    {
        SceneManager.LoadSceneAsync("MainLevel09");
    }
    public void MainLevel10()
    {
        SceneManager.LoadSceneAsync("MainLevel10");
    }
    public void MainLevel11()
    {
        SceneManager.LoadSceneAsync("MainLevel11");
    }
    public void MainLevel12()
    {
        SceneManager.LoadSceneAsync("MainLevel12");
    }
    public void MainLevel13()
    {
        SceneManager.LoadSceneAsync("MainLevel13");
    }
    public void MainLevel14()
    {
        SceneManager.LoadSceneAsync("MainLevel14");
    }
    public void MainLevel15()
    {
        SceneManager.LoadSceneAsync("MainLevel15");
    }

}
