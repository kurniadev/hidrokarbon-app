﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    public GameObject Profil, Infokeluar, Petunjuk, Pengaturan;



    public void Mulai()
    {
        SceneManager.LoadSceneAsync("Menu02");
    }
    public void MainMenu()
    {
        SceneManager.LoadSceneAsync("MainMenu");
    }

    public void Pendahuluan()
    {
        SceneManager.LoadSceneAsync("Pendahuluan");
    }
    public void Materi()
    {
        SceneManager.LoadSceneAsync("Materi");
    }
    public void KIKD()
    {
        SceneManager.LoadSceneAsync("KIKD");
    }
    public void TujuanPembelajaran()
    {
        SceneManager.LoadSceneAsync("TujuanPembelajaran");
    }
    public void LatihanSoal()
    {
        SceneManager.LoadSceneAsync("LatihanSoal");
    }
    public void Permainan()
    {
        SceneManager.LoadSceneAsync("MulaiGame");
    }


    // Back
    public void BackMenu02()
    {
        SceneManager.LoadSceneAsync("Menu01");
    }

    public void PendahuluanBack()
    {
        SceneManager.LoadSceneAsync("MainMenu");
    }
    public void MulaiGmaeBack()
    {
        SceneManager.LoadSceneAsync("MainMenu");
    }
    public void MateriBack()
    {
        SceneManager.LoadSceneAsync("MainMenu");
    }
    public void KIKDBack()
    {
        SceneManager.LoadSceneAsync("Pendahuluan");
    }
    public void TujuanPembelajaranBack()
    {
        SceneManager.LoadSceneAsync("Pendahuluan");
    }


    //Profile

    public void ButtonProfil()
    {
        Profil.SetActive(true);
    }
    public void CloseProfil()
    {
        Profil.SetActive(false);
    }

    // Info Keluar game
    public void ButtonKeluar()
    {
        Infokeluar.SetActive(true);
    }
    public void Buttontidakjadi()
    {
        Infokeluar.SetActive(false);
    }

    //Petujuk Penggunaan
    public void PetujukActive()
    {
        Petunjuk.SetActive(true);
    }
    public void PetujukClose()
    {
        Petunjuk.SetActive(false);
    }
    //Pengaturan
    public void PengaturanActive()
    {
        Pengaturan.SetActive(true);
    }
    public void PengaturanClose()
    {
        Pengaturan.SetActive(false);
    }



    public void KeluarApp()
    {
        Application.Quit();
    }

}
